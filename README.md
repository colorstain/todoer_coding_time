# Todoer

## Bootstrapping

- Install python3 - `brew install python3`
- Install virtualenvwrapper - `pip install virtualenvwrapper`
- Go to your project folder - `cd <wherever you have your project>`
- Create a virtual environment - `mkvirtualenv todoer --python python3`
- Install requirements - `pip install -r requirements.txt`


## Running your project

